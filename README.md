# TK-2000 System Software #
Software de apoio, device drivers, patches para firmwares e correlatos para este grande bastardo inglório da computação brazuca, o [TK-2000 da Microdigital](https://pt.wikipedia.org/wiki/TK2000).

## Conteúdo ###
Este repositório está dividido em duas seções principais, a saber:

* [Firmware](Firmware/README.md) - Hacks e patches para o Firmware do aparelho
* [Hardware](Hardware/README.md) - Device Drivers, hacks, mods e utilitários de suporte.

Cada seção contêm programas dividos em categorias, que por sua vez possuem a seguinte estrutura:

* Diretório de binários, _**bin**_, onde os programas pré-montados/compilados podem ser baixados para uso imediato. Normalmente o programa é apresentado em 3 formatos:
	* _**.bin**_ para ser copiado para a SDisk II;
	* _**.wav**_ para ser carregado pela porta cassete.

* Diretório de documentação, _**docs**_, onde a documentação de cada programa (quando existente) será encontrada.
* Diretório de fontes, _**souces**_, com o fonte dos programas.

Leia a documentação de cada seção/categoria para mais detalhes.

## Pré-requisitos ###
* Conhecimentos em:
	* Assembly 6502
	* Eletrônica Digital
	* Arquitetura do TK-2000
* Para o build do código
	* Gnu Make
		* Se usuário de Windows, ter instalado ao menos uma das opções abaixo:
			* coreutils da Gnu
			* Cygwin
			* MinGW
* Para o build do código 
	* [C2T Utils](https://github.com/fbelavenuto/ct2utils)
	* [CC65](http://www.cc65.org/)
* Para o deploy do código
	* Media Player ou Sound Editor para carregar os programas pela porta cassete do TK-2000
	* SDisk II recomendada, mas não exigida (exceto quando explicitamente mencionada).
	* Um TK-2000 funcionando. :-)

## Contatos e links ###

* Lisias 
	* ***_retro at lisias dot net_*** para contatos pessoais, sugestões, oferecimento de ajuda e papo furado. :-)
	* ***_support at lisias dot net_*** para suporte e relatório de erros. Adicionne `TK-2000` e a URL do componente no assunto do email, por favor.
* Comunidade no [Google Plus](https://plus.google.com/u/0/communities/101460215447733030037).

Por favor não peça suporte ou relate erros nas minhas contas pessoais, tampouco use o email de suporte para assuntos pessoais. Existe uma razão para eu ter criado duas contas diferentes. ;-)

Por favor compreenda que contactar me sobre este projeto usando qualquer outra forma de contato será solemente ignorada.
